(function() {
    class HelloWorld extends Sandstone.Component {
        constructor(element) {
            super(element);

            // Watch for clicks on the button and call toggle message when they happen
            this.callbacks = [
                [ "click", this.toggleMessage, "button" ]
            ];

            // Initialize the message
            this.state.message = "";
        }

        toggleMessage(e) {
            if(this.state.message.length) {
                this.state.message = "";
            } else {
                this.state.message = "Hello World";
            }
        }
    }

    Sandstone.register("hello-world", HelloWorld);
})();
