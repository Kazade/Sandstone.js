
# Sandstone.js

Sandstone.js is a simple reactive component system which allows progressive
enhancement of HTML content.

## What's special about Sandstone?

Sandstone:

 - Uses clean (and soon to be common place) ES6 class syntax
 - Allows you to apply components to DOM elements with a single data attribute (data-ss-component)
 - Allows purely logical components - you aren't required to render anything
 - Uses the existing DOM as the template, which makes tying into backend-rendered systems a breeze


## Usage

The first thing to do is create your component. You do this by subclassing "Sandstone.Component":

### Components and callbacks

```
class MyComponent extends Sandstone.Component {
    constructor(element) {
        super(element);
    }
}
```

Next, you must register your component with a name:

```
Sandstone.register("my-component", MyComponent);
```

You can then attach to a DOM node, like so:

```
<div data-ss-component="my-component"><button id="my-button">Click Me!</button></div>
```

When the DOM node is instantiated, a counterpart instance of MyComponent is created.

You can watch for events from child DOM nodes by setting the ".callbacks" attribute in the
component constructor.

```
class MyComponent extends Sandstone.Component {
    constructor(element) {
        super(element);
        this.callbacks = [
            [ "click", this.myHandler, "#my-button" ],
        ]
    }

    myHandler() {
        alert("Clicked!");
    }
}
```

### State and bindings

Every component has a "state" member variable. This state is built from the DOM element the
component is attached to and its children. You can define new state variables by using the following attributes:

 - data-ss-text

or you can define them yourself in your component's constructor:

    constructor(element) {
        super(element);
        self.state.myVariable = true;
    }

You can then make the DOM react to the state variable using the following attributes:

 - data-ss-text - Replaces the text content of the element with the specified state value
 - data-ss-visible - Shows/hides the element conditionally depending on the specified state value


State variables are not discovered below child components. For example:

    <div data-ss-component="parent">
        <p data-ss-text="var1"></p>
        <div data-ss-component="child">
            <p data-ss-text="var2"></p>
        </div>
    </div>

This will result in the parent component having a single "var1" state variable, and the
child component having a "var2" variable. However, the child component will also inherit a "parent"
state variable which provides access to the parent state. So you can do this:

    <div data-ss-component="parent">
        <p data-ss-text="var1"></p>
        <div data-ss-component="child">
            <!-- Display content from the parent state -->
            <p data-ss-text="parent.var1"></p>
        </div>
    </div>
