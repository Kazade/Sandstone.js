

var Sandstone = {};

Sandstone.useJQueryEvents = true;

Sandstone.register = function(identifier, klass) {
    Sandstone.registry.register(identifier, klass);
};

(function () {
    class ComponentRegistry {
        constructor() {
            this.componentRegistry = {};
            this.instanceRegistry = {};
            this.nextId = 0;

            var self = this;

            var deinitializeComponents = function (i) {
                var components = self.getcomponentsFromElement(this);

                components.forEach(function(component) {
                    component.deinit();
                });

                // Remove this element from the instance registry
                delete self.instanceRegistry[self.getElementUUID(this)];
            };

            /*
                Watch the document for node removals. If a node is removed then
                look for any components in its subtree and all deinit on them
                which will disconnect the signals
            */
            var observer = new MutationObserver(function(mutations) {

                for(var i = 0; i < mutations.length; ++i) {
                    var mutation = mutations[i];
                    for(var j = 0; j < mutation.removedNodes.length; ++j) {
                        var removedNode = mutation.removedNodes[j];
                        $(removedNode).find("[data-ss-component]").each(deinitializeComponents);
                    }
                }
            });

            observer.observe(document, {
                childList: true,
                attributes: false,
                subtree: true,
                characterData: false
            });

            this._observer = observer;
        }

        register(name, type) {
            this.componentRegistry[name] = type;
        }

        instantiate(element, name) {
            if (!(name in this.componentRegistry)) {
                throw 'component "' + name + '" is not registered';
            }

            // Ensure the requested component hasn't already been instantiated on
            // this DOM node.
            if (this.getcomponentsFromElement(element, name).length !== 0) {
                throw 'component "' + name + '" already exists on element';
            }

            var instance = new this.componentRegistry[name](element);

            var elementUUID = this.getElementUUID(element);

            if(elementUUID === null) {
                // Generate and apply a UUID for this element so we can
                // track it
                elementUUID = this.generateUUID();
                this.setElementUUID(element, elementUUID);

                // Then store the components
                this.instanceRegistry[elementUUID] = {
                    'node': element,
                    'components': [ instance ]
                };
            } else {
                // Append this component to this elements component list
                this.instanceRegistry[elementUUID].components.push(instance);
            }

            // Make sure we initialize any events etc.
            instance.init();
        }

        getcomponentsFromElement(element, name) {
            var elementUUID = this.getElementUUID(element);

            // Node has no components instantiated on it.
            if (typeof elementUUID === 'undefined' || this.instanceRegistry[elementUUID] === undefined) {
                return [];
            }

            var instantiatedcomponentInstances = this.instanceRegistry[elementUUID].components;

            if(name !== undefined) {
                instantiatedcomponentInstances = instantiatedcomponentInstances.filter(function (componentInstance) {
                    return componentInstance.type === name;
                });
            }

            return instantiatedcomponentInstances;
        }

        setElementUUID(element, uuid) {
            element.setAttribute('data-ss-uuid', uuid);
        }

        getElementUUID(element) {
            return element.getAttribute('data-ss-uuid');
        }

        initializeComponents(rootNode) {
            var self = this;
            $(rootNode).find("[data-ss-component]").addBack("[data-ss-component]").each(
                function initComponent(i, element) {
                    var componentNames = element.dataset.ssComponent.trim().split(' ');
                    componentNames.map(function(name) {
                        if(name) {
                            self.instantiate(element, name);
                        }
                    });
                }
            );
        }

        generateUUID() {
            return ':' + (this.nextId++).toString(36);
        }
    }

    Sandstone.registry = new ComponentRegistry();

    document.addEventListener("DOMContentLoaded", function(event) {
        Sandstone.registry.initializeComponents(document);
    });
})();

(function() {

    class State {
        constructor() {
            this._state = {};
            this._callbacks = {};
        }

        addItem(name, value) {
            var self = this;
            (function define(state, name) {
                Object.defineProperty(
                    state, name, {
                        get: function() { return state._getProperty(name); },
                        set: function(value) { state._setProperty(name, value); }
                    }
                );
            })(self, name);

            this[name] = value;
        }

        _getProperty(name) {
            return this._state[name];
        }

        _setProperty(name, value) {
            var originalValue = this._state[name];

            this._state[name] = value;

            var callbacks = this._callbacks[name];
            if(callbacks !== undefined) {
                for(var i = 0; i < callbacks.length; ++i) {
                    callbacks[i](name, originalValue, value);
                }
            }
        }

        watchForChange(name, callback) {
            if(this._callbacks[name] === undefined) {
                this._callbacks[name] = [ callback ];
            } else {
                this._callbacks[name].push(callback);
            }

            // Trigger the callback immediately so that existing state
            // is reflected
            callback(name, undefined, this._state[name]);
        }
    }

    function discoverState(element) {
        var result = new State();

        $("[data-ss-text]").each(function(i) {
            var $this = $(this);
            var stateVariable = $this.data("ss-text");

            result.addItem(stateVariable, $(this).text());
        });

        return result;
    }

    class Component {

        constructor(element) {
            this.callbacks = [];
            this.element = element;
            this.connectedEvents = [];
            this.state = discoverState(element);
            this.makeReactive();
        }

        makeReactive() {
            var self = this;

            $("[data-ss-text]").each(function(i) {
                var $this = $(this);
                var stateVariable = $this.data("ss-text");

                self.state.watchForChange(stateVariable, function(name, oldValue, newValue) {
                    $this.text(newValue);
                });
            });

            $("[data-ss-visible]").each(function(i) {
                var $this = $(this);
                var stateVariable = $this.data("ss-visible");

                self.state.watchForChange(stateVariable, function(name, oldValue, newValue) {
                    if(newValue.length > 0 || newValue) {
                        $this.show();
                    } else {
                        $this.hide();
                    }
                });
            });
        }

        addEventListener(element, event, callback) {
            if(Sandstone.useJQueryEvents) {
                $(element).on(
                    event,
                    null,
                    undefined,
                    callback
                );
            } else {
                element.addEventListener(event, callback);
            }
        }

        removeEventListener(element, event, callback) {
            if(Sandstone.useJQueryEvents) {
                $(element).off(event, null, callback);
            } else {
                element.removeEventListener(event, callback);
            }
        }

        connectEvents() {
            var connectEvent = function(event, callback, selector) {
                var callbackFunc = function(e) {
                    if((selector === null && e.target === self.element) || matches.call(e.target, selector)) {
                        /*
                            Sigh. So we want callbacks to allow class methods where 'this'
                            points to the instance of the class. But because triggering an
                            event binds 'this' to whatever was clicked or whatever, we need to switch
                            things around and bind the instance to 'this' and pass the event listener's
                            'this' as an additional argument after the event itself.
                        */
                        callback.bind(self)(e, this);
                    }
                };


                self.addEventListener(self.element, event, callbackFunc);

                console.log(callback);
                self.connectedEvents.push({
                    element: self.element,
                    event: event,
                    callback: callbackFunc
                });
            };

            for(var i = 0; i < this.callbacks.length; ++i) {
                var entry = this.callbacks[i];

                var event = entry[0];
                var callback = entry[1];
                var selector = entry[2];

                if(selector === undefined) {
                    selector = "*";
                }

                var self = this;

                var matches = this.element.matchesSelector ||
                              this.element.mozMatchesSelector ||
                              this.element.webkitMatchesSelector ||
                              this.element.oMatchesSelector ||
                              this.element.msMatchesSelector;

                connectEvent(event, callback, selector);
            }
        }

        disconnectEvents() {
            while(this.connectedEvents.length) {
                var entry = this.connectedEvents.pop();
                this.removeEventListener(entry.element, entry.event, entry.callback);
            }
        }

        init() {
            this.connectEvents();
        }

        deinit() {
            this.disconnectEvents();
        }
    }

    Sandstone.Component = Component;
})();
