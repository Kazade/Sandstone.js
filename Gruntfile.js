module.exports = function(grunt) {
    var bannerContent = '/*! <%= pkg.name %> v<%= pkg.version %> - \n' +
                    '<%= grunt.template.today("yyyy-mm-dd") %> \n' +
                    ' *  License: <%= pkg.license %> */\n';
    var name = '<%= pkg.name %>-v<%= pkg.version%>';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        esnext: {
            options: {},
            dist: {
                src: ['src/**/*.js'],
                dest: 'dist/' + name + '.js'
            }
        },

        concat: {
            options: {
                banner: bannerContent
            },
            target : {
                src : [ 'dist/' + name + '.js' ],
                dest : 'dist/' + name + '.js'
            }
        },

        jshint: {
            options: {
                trailing: true,
                eqeqeq: true,
                esnext: true
            },
            target: {
                src: [ 'src/**/*.js', 'tests/**/*.js' ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-esnext');
    grunt.registerTask('default', ['jshint', 'esnext', 'concat' ]);
};
